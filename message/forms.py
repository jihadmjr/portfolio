from django import forms
from .models import Message

class Message_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    dari = forms.CharField(label="Dari", required=True, max_length=20, widget=forms.TextInput(attrs=attrs))
    untuk = forms.CharField(label="Untuk", required=True, max_length=20, widget=forms.TextInput(attrs=attrs))
    message = forms.CharField(label="Pesan", required=True, max_length=100, widget=forms.TextInput(attrs=attrs))

