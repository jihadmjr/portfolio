from django.http import HttpResponseRedirect
from django.middleware import http
from django.shortcuts import render
from .forms import Message_Form
from .models import Message

response = {}
# Create your views here.
def index(request):
    lokasi_template = 'message/form.html'
    message = Message.objects.all()
    form = Message_Form(request.POST)
    response['form'] = form
    response['list'] = message
    if(request.method == 'POST' and form.is_valid()):
        response['dari'] = request.POST['dari']
        response['untuk'] = request.POST['untuk']
        response['message'] = request.POST['message']

        message = Message.objects.create(dari=response['dari'], untuk=response['untuk'], message=response['message'])
        message.save
        return HttpResponseRedirect('/message/')

    return render(request, lokasi_template, response)