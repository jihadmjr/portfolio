from django.db import models

# Create your models here.
class Message(models.Model):

    dari = models.CharField(max_length=20, null=False)
    untuk = models.CharField(max_length=20, null=False)
    message = models.TextField(max_length=180, null=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.message
