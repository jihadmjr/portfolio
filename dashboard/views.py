from django.shortcuts import render

response = {'Name': 'Jihad Rinaldi'}


# Create your views here.
def index(request):
    lokasi_template = 'dashboard/index.html'
    return render(request, lokasi_template, response)
